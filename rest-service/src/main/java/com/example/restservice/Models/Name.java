package com.example.restservice.Models;

public class Name {
	
	private String content;
	private int occurrences;
	
	public Name(String content, int occurrences) {
		this.content = content;
		this.occurrences = occurrences;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String value) {
		this.content = value;
	}

	public int getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}

}
