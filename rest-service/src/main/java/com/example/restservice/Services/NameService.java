package com.example.restservice.Services;

import com.example.restservice.Models.Name;
import com.example.restservice.Repositories.NameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NameService implements INameService {

    @Autowired
    private NameRepository nameRepository;
    private List<Name> listOfNames; //include Model here or in Repository? (Couldn't find best location for this)
    private String oliverTwistText;

    public NameService(NameRepository nameRepository) {
        this.nameRepository = nameRepository;

        listOfNames = loadNamesList();
        oliverTwistText = this.nameRepository.loadOliverTwistText(); //only want to load once at app startup

        listOfNames = this.nameRepository.getNoOfOccurrencesInText(oliverTwistText, listOfNames);
        listOfNames = this.nameRepository.sortByNoOfOccurrencesDesc(listOfNames);

        save(listOfNames);

    }

    public void save(Name name) {
        nameRepository.save(name);
    }

    public void save(List<Name> names) {
        nameRepository.save(names);
    }

    public List<Name> loadNamesList() {
        return nameRepository.loadNamesList();
    }

    @Override
    public Name getNoOfOccurrencesInText(String name) {//with for loop code, returns 0 occurrences if not in listOfNames
        Name nameFromRequest = new Name(name, 0);

        for (int i = 0; i < listOfNames.size(); i++) { //check if name exists in listOfNames
            if (listOfNames.get(i).getContent().equals(name)){

                //if name is in list and occurrences have been calculated, don't search oliver-twist.txt again, get occurrences from name in list
                nameFromRequest = listOfNames.get(i);
                break;
            }
        }

        //code to check any name exists in oliver-twist.txt, doesn't check if name is in listOfNames
        //requestedName = new Name(name, nameRepository.getNoOfOccurrencesInText(oliverTwistText, name));

        return nameFromRequest;
    }
}
