package com.example.restservice.Services;

import com.example.restservice.Models.Name;

import java.util.List;

public interface INameService {
    public abstract Name getNoOfOccurrencesInText(String name);
    public abstract void save(Name name);
    public abstract void save(List<Name> names);
    public abstract List<Name> loadNamesList();
}
