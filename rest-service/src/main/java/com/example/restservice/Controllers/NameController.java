package com.example.restservice.Controllers;

import com.example.restservice.Models.Name;
import com.example.restservice.Services.NameService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NameController {

	private NameService nameService;

	public NameController(NameService nameService) {
		this.nameService = nameService;
	}

	@GetMapping("/name-count/{name}")
	public Name nameCount(@PathVariable("name") String name) {
		return nameService.getNoOfOccurrencesInText(name);
	}
}