package com.example.restservice.Repositories;

import com.example.restservice.Models.Name;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.CrudRepository; //included to show other attempted solution
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class NameRepository implements INameRepository {

    @Override
    public List<Name> sortByNoOfOccurrencesDesc(List<Name> unsortedNames) {

        List<Name> sortedNames = unsortedNames.stream()
                .sorted(Comparator.comparing(Name::getOccurrences).reversed())
                .collect(Collectors.toList());

        return sortedNames;
    }

    @Override
    public int getNoOfOccurrencesInText(String textToSearch, String name) {
        return StringUtils.countOccurrencesOf(textToSearch, name);
    }

    public List<Name> getNoOfOccurrencesInText(String textToSearch, List<Name> names) {
        for (int i = 0; i < names.size(); i++) {
            names.get(i).setOccurrences(StringUtils.countOccurrencesOf(textToSearch, names.get(i).getContent()));
        }
        return names;
    }

    @Override
    public void save(Name name) {

    }

    @Override
    public void save(List<Name> names) {
        //alternative solution: manually create file and reference it for writing contents to it
        try {
            File resourcesStaticFolder = new ClassPathResource("/static").getFile();
            File nameAndOccurrencesDescFile = new File(resourcesStaticFolder + "/name-and-occurrences-desc.txt");

            if (nameAndOccurrencesDescFile.createNewFile()) {
                System.out.println("File created: " + nameAndOccurrencesDescFile.getAbsolutePath());
            }

            PrintWriter printWriter = new PrintWriter (nameAndOccurrencesDescFile);

            for (int i = 0; i < names.size(); i++) {
                printWriter.println(names.get(i).getContent() + ": " + names.get(i).getOccurrences());
            }

            printWriter.close();

            System.out.println("Successfully wrote to name-and-occurrences-desc.txt.");

        } catch (IOException e) {
            System.out.println("An error occurred while saving names to name-and-occurrences-desc.txt.");
            e.printStackTrace();
        }
    }

    public List<Name> loadNamesList() {
        List<Name> loadedNames = new ArrayList<>();

        Resource resource = new ClassPathResource("/static/first-names.txt"); //inject path name as oppose to hard coded?

        try {
            InputStream inputStream = resource.getInputStream();
            InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader);
            for (String line; (line = reader.readLine()) != null;) {
                loadedNames.add(new Name(line, 0));
                System.out.println(loadedNames.get(loadedNames.size() - 1).getContent());
            }

        } catch (IOException e) {
            System.out.println("error reading names from file.");
        }

        return loadedNames;
    }

    public String loadOliverTwistText() {
        String oliverTwistText = "";

        Resource resource = new ClassPathResource("/static/oliver-twist.txt"); //inject path name as oppose to hard coded?

        System.out.println("\nLoading Oliver Twist text...\n");
        try {
            InputStream inputStream = resource.getInputStream();
            InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader);
            for (String line; (line = reader.readLine()) != null;) {
                oliverTwistText += line;
            }

        } catch (IOException e) {
            System.out.println("error reading Oliver Twist text from file.");
        }
        System.out.println("loaded text:" + oliverTwistText);

        return oliverTwistText;
    }
}
