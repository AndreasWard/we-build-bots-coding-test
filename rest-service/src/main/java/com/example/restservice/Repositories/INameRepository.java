package com.example.restservice.Repositories;

import com.example.restservice.Models.Name;

import java.util.List;

public interface INameRepository { //could extend CrudRepository or JPARepository (however require save to text file code)
    public List<Name> sortByNoOfOccurrencesDesc(List<Name> unsortedNames);
    public int getNoOfOccurrencesInText(String textToSearch, String name);
    public void save(Name name);
    public void save(List<Name> names);
    public abstract List<Name> loadNamesList();

}
