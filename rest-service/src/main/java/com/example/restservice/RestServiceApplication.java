package com.example.restservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Read txt file, parse and count no. of first name occurrences in 'oliver-twist.txt'
 * no. of occurrences should be sorted in desc order + written to text file in provided example format
 * false positives, e.g. Sunday and Mercy included in names text file but not incl in oliver-twist.txt... not req to add support for these
 * 
 * ENDPOINT REQ:
 * 	/name-count (params: name) returns JSON object with 2 fields... name and no. of occurrences
 */

@SpringBootApplication
public class RestServiceApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(RestServiceApplication.class);
		app.run(args);
	}
}